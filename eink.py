#imported some code from lerem.py written by https://github.com/oprema/Waveshare-E-Ink.git thanks to you!!!

import spidev
import RPi.GPIO as GPIO
import time
from PIL import Image
import os
from PIL import ImageDraw
from PIL import ImageFont
import textwrap

# Display resolution
EPD_WIDTH       = 152
EPD_HEIGHT      = 152
#from pdf
PANEL_SET               = 0x00
POWER_OFF               = 0x02
POWER_ON                = 0x04
BOOST_SOFT_START        = 0x06
DEEP_SLEEP              = 0x07
DATA_START_BLACK        = 0x10
DATA_STOP               = 0x11
DISPLAY_REFRESH         = 0x12
DATA_START_YELLOW       = 0x13
VCOM_SET                = 0x50
RESOLUTION_SET          = 0x61
PARTIAL_WINDOW          = 0x90
PARTIAL_IN              = 0x91
PARTIAL_OUT             = 0x92

# Pin definitions
RST = 17        # pin 11
DC = 25         # pin 22
BUSY = 24       # pin 18
CS = 8          # pin 24


WHITE = 1
BLACK = 0
FONT_PATH = '/usr/share/fonts/truetype/freefont/'
FONT_DEFAULT = 'FreeMonoBold.ttf'
FONT_SIZE = 20

class Text(object):
  def __init__(self, width, height, text, xtext=6, ytext=6, chars=14, font_file=None):
    image = Image.new('1', (width, height), WHITE)
    if font_file == None:
      font_file = FONT_DEFAULT
    font = ImageFont.truetype(FONT_PATH + font_file, FONT_SIZE)

    draw = ImageDraw.Draw(image)
    w = textwrap.TextWrapper(width=chars, break_long_words=True) #, replace_whitespace=False)
    for line in text.splitlines(): 
      for frag in w.wrap(line):
        #print("Line: '%s'" % frag)
        width, height = font.getsize(frag)
        draw.text((xtext, ytext), frag, font=font, fill=BLACK)
        ytext += height
    self._image = image #.transpose(Image.ROTATE_90)

  @property
  def image(self):
    return self._image


height=EPD_HEIGHT
width =EPD_WIDTH

# canvas to draw to
image = Image.new('1',(height,width), WHITE)

# add some text to it
text = Text(width, height, "ciao a tutta\nla cummunity\ndi freecircle.", chars=24)
image.paste(text.image, (0, 0, height, width), mask=BLACK)

# send image to display
image.save("/home/pi/epd154/test.bmp")



# Initialize pins
GPIO.setmode(GPIO.BCM)		#set the GPIO mode according to the BCM numeration non the PIN numbering
GPIO.setwarnings(False)
GPIO.setup(RST, GPIO.OUT)
GPIO.setup(DC, GPIO.OUT)
GPIO.setup(CS, GPIO.OUT)
GPIO.setup(BUSY, GPIO.IN)


# Initialize SPI
spi=spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz = 1953000 #(1953 khz reale)
#spi.max_speed_hz = 1953 #(1953 khz reale)
spi.mode = 0b00


def partial_definition(xy,xy1):
    #the x and y coordinates must be 8 x value then hrst and hred must be an ineger beetween 0x00 and 0x13
    hrst=int(xy[0])
    vrst=xy[1]
    hred = int(xy1[0])
    vred = xy1[1]
    payload=[]
    print("angolo superiore sinistro %s ,%s , inferiore destro %s , %s" %(hrst,vrst,hred,vred))
    payload1=hrst & 0b11111000
    payload2=(hred & 0b11111000) | 0b00000111
    if (vrst > 256):
        temp= vrst -256
        payload3 = temp & 0b00000001
    else:
       payload3 = 0x00
    payload4= vrst & 0b00011111111
    if (vred > 256):
        temp= vred -256
        payload5 = temp & 0b00000001
    else:
       payload5 = 0x00
    payload6= vred & 0b00011111111
    payload7=0x00 # 0: Gates scan only inside of the partial window.
                 #      1: Gates scan both inside and outside of the partial window. (default)
    payload.append(payload1)
    payload.append(payload2)
    payload.append(payload3)
    payload.append(payload4)
    payload.append(payload5)
    payload.append(payload6)
    payload.append(payload7)
    return payload

 

def delay_ms(delaytime):
    time.sleep(delaytime/1000.0)


def SendCommand(command):
    lista=[]
    lista.append(command)
    GPIO.output(DC,GPIO.LOW)
    spi.xfer2(lista)
    GPIO.output(DC,GPIO.HIGH)
    #delay_ms(0.1)

def SendData(data):
    lista=[]
    lista.append(data)
    spi.xfer2(lista)
    #delay_ms(0.1)

def set_APG():
    SendCommand(0xA1)


def reset():
    GPIO.output(RST, GPIO.HIGH)
    delay_ms(200)
    GPIO.output(RST, GPIO.LOW)
    delay_ms(200)
    GPIO.output(RST, GPIO.HIGH)
    delay_ms(200)

def digital_write(pin, value):
    GPIO.output(pin, value)

def digital_read(pin):
    return GPIO.input(BUSY_PIN)

def wait_busy():
    a=0
    while(GPIO.input(BUSY) == 0 and a < 50):
        print("while %s"%a)
        a+=1
        delay_ms(100)

def TurnOnDisplay():
    SendCommand(BOOST_SOFT_START)  #0x06
    SendData(0x17)
    SendData(0x17)
    SendData(0x17)

def powerON():
    SendCommand(POWER_ON)  #0x04
    wait_busy()

def panelSet():
    SendCommand(PANEL_SET) #0x00
    SendData(0x0f)
    #SendData(0x0d)   #lut from register if setted doesn't change the display 


def resolution():
    SendCommand(RESOLUTION_SET) #0x61
    SendData(0x98)	#bit D7 - D3 HRES 98 = 152
    SendData(0x00)	#bit D0 - msb bit VRES
    SendData(0x98) 	#bit D7 - D0  bit VRES 98 = 152

def vcom():
    SendCommand(VCOM_SET)  	#0x50
    SendData(0x77)		#more investigate this data

def deep_sleep():
    SendCommand(0x07)       #deep sleep

    SendData(0xa5)	#checksum data

def design_whole():

    print("inizio il reset")
    reset()
    delay_ms(500)
    print("reset finito")
    print("inizio turn on on")

    TurnOnDisplay()         #0x06 0x17 0x17 0x17

    print("inizio power on")
    powerON()               #0x04 0x71

    wait_busy()

    panelSet()              #0x00 0x07

    resolution()            #0x61 0x98 0x00 0x98

    vcom()                  #0x50 0x77

    print("inizio invio dati")
    SendCommand(DATA_START_BLACK)   #0x10
    b=0
    area= int(EPD_WIDTH * EPD_HEIGHT / 8)
    print ("the area is %s byte" % area)
    for a in range(area):
        SendData(0xFF)
    SendCommand(DATA_STOP) #0x11

    SendCommand(DATA_START_YELLOW)  #0x13
    for a in range(area):
        SendData(0x00)
    SendCommand(DATA_STOP)  #0x11
    print("fine invio dati")

    SendCommand(DISPLAY_REFRESH)       #0x12 display refresh

    delay_ms(28000)
    wait_busy()

    vcom()                  #0x50 0x77

    SendCommand(POWER_OFF)       #0x02 power off

    wait_busy()

    deep_sleep()

    print("chiudo la comunicazione")



'''
we will try to select a rectangular area with the xy , x1y1  coordinates
to pass to the finction windows partial
then start the windows mode , send the data and exit from the windows patial mode
'''

def design_partial():

    print("inizio il reset")
    reset()
    delay_ms(500)
    print("reset finito")
    print("inizio turn on on")

    TurnOnDisplay()         #0x06 0x17 0x17 0x17

    print("inizio power on")
    powerON()               #0x04 0x71

    wait_busy()

    panelSet()              #0x00 0x07

    resolution()            #0x61 0x98 0x00 0x98

    vcom()                  #0x50 0x77

    ''' 
    put here the partial design
    '''
    xy=[0,0]
    xy1=[152,40]
    payload=partial_definition(xy,xy1)
    SendCommand(PARTIAL_WINDOW)
    for pay in payload:
        SendData(pay)
    wait_busy()
    SendCommand(PARTIAL_IN)
    print("inizio invio dati")
    SendCommand(DATA_START_BLACK)   #0x10
    b=0
    area = int((xy1[0]-xy[0]) * (xy1[1]-xy[1]) / 8)
    print("the areai is %s byte" % area)
    for a in range(area):
        SendData(0x00)
    SendCommand(DATA_STOP) #0x11
    wait_busy()
    SendCommand(DATA_START_YELLOW)  #0x13
    for a in range(area):
        SendData(0xff)
    SendCommand(DATA_STOP)  #0x11
    print("fine invio dati")
    wait_busy()
    SendCommand(PARTIAL_OUT)
    wait_busy()


    SendCommand(DISPLAY_REFRESH)       #0x12 display refresh

    delay_ms(28000)
    wait_busy()

    vcom()                  #0x50 0x77

    SendCommand(POWER_OFF)       #0x02 power off

    wait_busy()

    deep_sleep()

    print("chiudo la comunicazione")


def write_text(image):

    print("inizio il reset")
    reset()
    delay_ms(500)
    print("reset finito")
    print("inizio turn on on")

    TurnOnDisplay()         #0x06 0x17 0x17 0x17

    print("inizio power on")
    powerON()               #0x04 0x71

    wait_busy()

    panelSet()              #0x00 0x07

    resolution()            #0x61 0x98 0x00 0x98

    vcom()                  #0x50 0x77

    print("inizio invio dati")
    SendCommand(DATA_START_BLACK)   #0x10
    b=0
    area=152*152/8
    print(area)
    immagine=image.tobytes(encoder_name="raw")
    for a in immagine:
        SendData(a)
        print(a,end=" ")
        b+=1
    SendCommand(DATA_STOP) #0x11
    print(b)
    wait_busy()
    SendCommand(DATA_START_YELLOW)  #0x13
    index=0
    img=[]
    for a in immagine:
        index+=1
        img.append(a)
        if(index<2888/2):
            SendData(0xFF) #trasparente
        else:
            SendData(img[index-int(2888/2)])
    SendCommand(DATA_STOP)  #0x11
    print("fine invio dati")
    wait_busy()


    SendCommand(DISPLAY_REFRESH)       #0x12 display refresh

    delay_ms(28000)
    wait_busy()

    vcom()                  #0x50 0x77

    SendCommand(POWER_OFF)       #0x02 power off
    wait_busy()

    deep_sleep()

    print("chiudo la comunicazione")

while True:

    print("menu selection :")
    print("1) to deisgnt the whole display")
    print("2) to design only a partial area\n")
    print("3) write text")
    print("9) to exit\n")

    choice= int(input("choose 1 or 2\n"))
    if choice == 1:
        try:
            design_whole()
        except:
            print("quit for an error")
            quit()
    elif choice == 2:
        try:
            design_partial()
        except: 
            print("quit for an error")
            quit()
    elif choice == 3:
        try:
            write_text(image)
        except Exception as e:
            print(e) 
            print("quit for an error")
            quit()

    else:
        print("exit no choice selected .....")
        spi.close()
        quit()
