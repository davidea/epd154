# epd154

a simple python3 script to use the waveshare 1.54 inch bi colour e-ink onto the raspberry pi, since the waveshare project doesn't work

at this early stage the script open the comunication with the display , and send some data 0xF0 for the black and white image and 0x0F for the yellow and white display 
(my unit is a bicolor black and yellow display)


the connection with the raspberry is :

Display   Raspi

3.3V      3.3V - Pin 17

GND       GND  - Pin 20

DIN       MOSI - Pin 19         output data from the raspberry , input data onto the display

CLK       SCLK - Pin 23

CS        CE0  - Pin 24         chip select , the raspberry support 2 spi device CE0 pin 24 and CE1 pin 26

DC        GPIO 25 - Pin 22      data/command selection  0 write command , 1 write data

RST       GPIO 17 - Pin 11      reset active low

BUSY      GPIO 24 - Pin 18      active low


from the module specification :


This pin (BUSY) is Busy state output pin. When Busy is Low, the operation of chip should not be interrupted and any
commands should not be issued to the module. The driver IC will put Busy pin Low when the driver IC is working such as:
- Outputting display waveform; or
- Programming with OTP
- Communicating with digital temperature sensor


maybe will be better in the future to check everey command if this pin is busy



some general info on the spi comunication:

 You can adjust border color by controlling 0x3C register.

There are 4 SPI communication modes. SPI0 is commonly used, in which CPHL =0, CPOL = 0.

data is transferred by bits, MSB.

put CS low , then set DC
finally send command and data


